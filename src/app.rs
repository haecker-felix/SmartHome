// SmartHome - app.rs
// Copyright (C) 2020  Felix Häcker <haeckerfelix@gnome.org>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use futures_util::FutureExt;
use gio::subclass::prelude::ApplicationImpl;
use gio::{self, prelude::*, ApplicationFlags, SettingsExt};
use glib::subclass;
use glib::subclass::prelude::*;
use glib::translate::*;
use glib::{Receiver, Sender};
use gtk::prelude::*;
use gtk::subclass::application::GtkApplicationImpl;

use std::cell::RefCell;
use std::env;
use std::rc::Rc;
use std::str::FromStr;

use crate::api::{Client, ClientCredentials, EntityModel, WebSocketClient, WebSocketMessage};
use crate::config;
use crate::secret::SecretManager;
use crate::settings::{settings_manager, Key};
use crate::ui::ShApplicationWindow;

#[derive(Debug, Clone)]
pub enum Action {
    Login(ClientCredentials),
    Logout,

    ProcessWebSocketMessage(WebSocketMessage),

    RefreshData,
    SettingsKeyChanged(Key),
}

pub struct ShApplicationPrivate {
    sender: Sender<Action>,
    receiver: RefCell<Option<Receiver<Action>>>,

    window: RefCell<Option<ShApplicationWindow>>,
    client: Rc<Client>,

    websocket_client: Rc<WebSocketClient>,
    entity_model: RefCell<EntityModel>,

    settings: gio::Settings,
}

impl ObjectSubclass for ShApplicationPrivate {
    const NAME: &'static str = "ShApplication";
    type ParentType = gtk::Application;
    type Instance = subclass::simple::InstanceStruct<Self>;
    type Class = subclass::simple::ClassStruct<Self>;

    glib_object_subclass!();

    fn new() -> Self {
        let (sender, r) = glib::MainContext::channel(glib::PRIORITY_DEFAULT);
        let receiver = RefCell::new(Some(r));

        let window = RefCell::new(None);
        let client = Rc::new(Client::new());

        let websocket_client = WebSocketClient::new(sender.clone());
        let entity_model = RefCell::new(EntityModel::new());

        let settings = settings_manager::get_settings();

        Self {
            sender,
            receiver,
            window,
            client,
            websocket_client,
            entity_model,
            settings,
        }
    }
}

// Implement GLib.OBject for ShApplication
impl ObjectImpl for ShApplicationPrivate {
    glib_object_impl!();
}

// Implement Gtk.Application for ShApplication
impl GtkApplicationImpl for ShApplicationPrivate {}

// Implement Gio.Application for ShApplication
impl ApplicationImpl for ShApplicationPrivate {
    fn activate(&self, _app: &gio::Application) {
        debug!("gio::Application -> activate()");

        // If the window already exists,
        // present it instead creating a new one again.
        if let Some(ref window) = *self.window.borrow() {
            window.present();
            info!("Application window presented.");
            return;
        }

        // No window available -> we have to create one
        let app = ObjectSubclass::get_instance(self).downcast::<ShApplication>().unwrap();
        let window = app.create_window();
        window.present();
        self.window.replace(Some(window));
        info!("Created application window.");

        // Setup action channel
        let receiver = self.receiver.borrow_mut().take().unwrap();
        receiver.attach(None, move |action| app.process_action(action));

        // Setup settings signal (we get notified when a key gets changed)
        self.settings.connect_changed(clone!(@strong self.sender as sender => move |_, key_str| {
            let key: Key = Key::from_str(key_str).unwrap();
            send!(sender, Action::SettingsKeyChanged(key));
        }));

        // Check if credentials are available
        if let Some(credentials) = ShApplication::get_credentials() {
            send!(self.sender, Action::Login(credentials));
        } else {
            self.window.borrow().as_ref().unwrap().show_login_page(true);
        }

        // List all setting keys
        settings_manager::list_keys();
    }
}

// Wrap ShApplicationPrivate into a usable gtk-rs object
glib_wrapper! {
    pub struct ShApplication(
        Object<subclass::simple::InstanceStruct<ShApplicationPrivate>,
        subclass::simple::ClassStruct<ShApplicationPrivate>,
        ShApplicationClass>)
        @extends gio::Application, gtk::Application;

    match fn {
        get_type => || ShApplicationPrivate::get_type().to_glib(),
    }
}

// ShApplication implementation itself
impl ShApplication {
    pub fn run() {
        info!("{} ({}) ({})", config::NAME, config::APP_ID, config::VCS_TAG);
        info!("Version: {} ({})", config::VERSION, config::PROFILE);
        info!("Isahc version: {}", isahc::version());

        // Create new GObject and downcast it into ShApplication
        let app = glib::Object::new(ShApplication::static_type(), &[("application-id", &Some(config::APP_ID)), ("flags", &ApplicationFlags::empty())])
            .unwrap()
            .downcast::<ShApplication>()
            .unwrap();

        // Start running gtk::Application
        let args: Vec<String> = env::args().collect();
        ApplicationExtManual::run(&app, &args);
    }

    fn create_window(&self) -> ShApplicationWindow {
        let self_ = ShApplicationPrivate::from_instance(self);
        let window = ShApplicationWindow::new(self_.sender.clone(), self.clone(), self_.client.clone());

        // Load custom styling
        let p = gtk::CssProvider::new();
        gtk::CssProvider::load_from_resource(&p, "/de/haeckerfelix/SmartHome/gtk/style.css");
        gtk::StyleContext::add_provider_for_screen(&gdk::Screen::get_default().unwrap(), &p, 500);

        // Setup help overlay
        let builder = gtk::Builder::new_from_resource("/de/haeckerfelix/SmartHome/gtk/shortcuts.ui");
        get_widget!(builder, gtk::ShortcutsWindow, shortcuts);
        window.set_help_overlay(Some(&shortcuts));

        window
    }

    fn login(&self, credentials: ClientCredentials) {
        let self_ = ShApplicationPrivate::from_instance(self);

        // Store server url in gsettings
        settings_manager::set_string(Key::ApiServer, credentials.server.to_string());

        // Store token in keyring
        SecretManager::store_credentials(credentials.clone()).unwrap();

        self_.websocket_client.set_credentials(Some(credentials.clone()));
        spawn!(self_.websocket_client.clone().connect().map(|_| {}));

        self_.client.set_credentials(Some(credentials));
        self_.window.borrow().as_ref().unwrap().show_login_page(false);

        send!(self_.sender, Action::RefreshData);
    }

    fn logout(&self) {
        let self_ = ShApplicationPrivate::from_instance(self);
        let url = url::Url::parse(&settings_manager::get_string(Key::ApiServer)).unwrap();

        // Remove token from keyring
        SecretManager::reset_credentials(url).unwrap();

        // Remove server url from gsettings
        settings_manager::set_string(Key::ApiServer, "".to_string());

        self_.websocket_client.set_credentials(None);
        spawn!(self_.websocket_client.clone().disconnect().map(|_| {}));

        self_.client.set_credentials(None);
        self_.window.borrow().as_ref().unwrap().show_login_page(true);
    }

    fn get_credentials() -> Option<ClientCredentials> {
        let server_url = url::Url::parse(&settings_manager::get_string(Key::ApiServer)).ok()?;
        SecretManager::get_credentials(server_url)
    }

    fn process_action(&self, action: Action) -> glib::Continue {
        let self_ = ShApplicationPrivate::from_instance(self);

        match action {
            Action::Login(config) => self.login(config),
            Action::Logout => self.logout(),
            Action::ProcessWebSocketMessage(message) => match message {
                WebSocketMessage::States(states) => self_.entity_model.borrow_mut().set_states(states),
                WebSocketMessage::Services(services) => self_.entity_model.borrow_mut().set_services(services),
            },
            Action::RefreshData => self_.window.borrow().as_ref().unwrap().refresh_data(),
            Action::SettingsKeyChanged(key) => {
                debug!("Settings key changed: {:?}", &key);
            }
        }
        glib::Continue(true)
    }
}
