// SmartHome - window.rs
// Copyright (C) 2020  Felix Häcker <haeckerfelix@gnome.org>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use gio::prelude::*;
use glib::subclass;
use glib::subclass::prelude::*;
use glib::translate::*;
use glib::Sender;
use gtk::prelude::*;
use gtk::subclass::prelude::{ApplicationWindowImpl, BinImpl, ContainerImpl, WidgetImpl, WindowImpl};
use libhandy::prelude::*;
use libhandy::SqueezerExt;

use std::cell::RefCell;
use std::rc::Rc;

use crate::api::Client;
use crate::app::{Action, ShApplication};
use crate::config;
use crate::settings::{settings_manager, Key, SettingsWindow};
use crate::ui::pages::{DashboardPage, EntitiesPage, HomeInfoPage, LoginPage, Page};
use crate::ui::{about_dialog, Notification};

pub struct ShApplicationWindowPrivate {
    window_builder: gtk::Builder,
    menu_builder: gtk::Builder,

    pages: RefCell<Vec<Box<dyn Page>>>,
    current_notification: RefCell<Option<Rc<Notification>>>,
}

impl ObjectSubclass for ShApplicationWindowPrivate {
    const NAME: &'static str = "ShApplicationWindow";
    type ParentType = gtk::ApplicationWindow;
    type Instance = subclass::simple::InstanceStruct<Self>;
    type Class = subclass::simple::ClassStruct<Self>;

    glib_object_subclass!();

    fn new() -> Self {
        let window_builder = gtk::Builder::new_from_resource("/de/haeckerfelix/SmartHome/gtk/window.ui");
        let menu_builder = gtk::Builder::new_from_resource("/de/haeckerfelix/SmartHome/gtk/menu/app_menu.ui");

        let pages = RefCell::new(Vec::new());
        let current_notification = RefCell::new(None);

        Self {
            window_builder,
            menu_builder,
            pages,
            current_notification,
        }
    }
}

// Implement GLib.OBject for ShApplicationWindow
impl ObjectImpl for ShApplicationWindowPrivate {
    glib_object_impl!();
}

// Implement Gtk.Widget for ShApplicationWindow
impl WidgetImpl for ShApplicationWindowPrivate {}

// Implement Gtk.Container for ShApplicationWindow
impl ContainerImpl for ShApplicationWindowPrivate {}

// Implement Gtk.Bin for ShApplicationWindow
impl BinImpl for ShApplicationWindowPrivate {}

// Implement Gtk.Window for ShApplicationWindow
impl WindowImpl for ShApplicationWindowPrivate {}

// Implement Gtk.ApplicationWindow for ShApplicationWindow
impl ApplicationWindowImpl for ShApplicationWindowPrivate {}

// Wrap ShApplicationWindowPrivate into a usable gtk-rs object
glib_wrapper! {
    pub struct ShApplicationWindow(
        Object<subclass::simple::InstanceStruct<ShApplicationWindowPrivate>,
        subclass::simple::ClassStruct<ShApplicationWindowPrivate>,
        ShApplicationWindowClass>)
        @extends gtk::Widget, gtk::Container, gtk::Bin, gtk::Window, gtk::ApplicationWindow;

    match fn {
        get_type => || ShApplicationWindowPrivate::get_type().to_glib(),
    }
}

// ShApplicationWindow implementation itself
impl ShApplicationWindow {
    pub fn new(sender: Sender<Action>, app: ShApplication, client: Rc<Client>) -> Self {
        // Create new GObject and downcast it into ShApplicationWindow
        let window = glib::Object::new(ShApplicationWindow::static_type(), &[]).unwrap().downcast::<ShApplicationWindow>().unwrap();

        app.add_window(&window.clone());
        window.setup_widgets(sender.clone(), client.clone());
        window.setup_signals();
        window.setup_gactions(sender);
        window
    }

    pub fn setup_widgets(&self, sender: Sender<Action>, client: Rc<Client>) {
        let self_ = ShApplicationWindowPrivate::from_instance(self);

        // Add headerbar/content to the window itself
        get_widget!(self_.window_builder, libhandy::HeaderBar, headerbar);
        get_widget!(self_.window_builder, gtk::Overlay, content);
        self.set_titlebar(Some(&headerbar));
        self.add(&content);

        // Set hamburger menu
        get_widget!(self_.menu_builder, gtk::PopoverMenu, popover_menu);
        get_widget!(self_.window_builder, gtk::MenuButton, appmenu_button);
        appmenu_button.set_popover(Some(&popover_menu));

        // Add devel style class for development or beta builds
        if config::PROFILE == "development" || config::PROFILE == "beta" {
            let ctx = self.get_style_context();
            ctx.add_class("devel");
        }

        // Restore window geometry
        let width = settings_manager::get_integer(Key::WindowWidth);
        let height = settings_manager::get_integer(Key::WindowHeight);
        self.resize(width, height);

        // Dashboard page
        let dashboard_page = DashboardPage::new(sender.clone(), client.clone());
        self.add_page(Box::new(dashboard_page));

        // States page
        let entities_page = EntitiesPage::new(sender.clone(), client.clone());
        self.add_page(Box::new(entities_page));

        // Home Info page
        let home_info_page = HomeInfoPage::new(sender.clone(), client.clone());
        self.add_page(Box::new(home_info_page));

        // Login page
        let login_page = LoginPage::new(sender.clone());
        self.add_page(Box::new(login_page));
    }

    fn setup_signals(&self) {
        let self_ = ShApplicationWindowPrivate::from_instance(self);

        // switch between mobile / desktop mode
        get_widget!(self_.window_builder, libhandy::Squeezer, squeezer);
        get_widget!(self_.window_builder, libhandy::ViewSwitcherBar, view_switcher_bar);
        squeezer.connect_property_visible_child_notify(clone!(@weak view_switcher_bar => move |squeezer|{
            let name = squeezer.get_visible_child().unwrap().get_widget_name().unwrap();
            let show = name == "GtkLabel";
            view_switcher_bar.set_reveal(show);
        }));

        // dark mode
        let s = settings_manager::get_settings();
        let gtk_s = gtk::Settings::get_default().unwrap();
        s.bind("dark-mode", &gtk_s, "gtk-application-prefer-dark-theme", gio::SettingsBindFlags::GET);

        // window gets closed
        self.connect_delete_event(move |window, _| {
            debug!("Saving window geometry.");
            let width = window.get_size().0;
            let height = window.get_size().1;

            settings_manager::set_integer(Key::WindowWidth, width);
            settings_manager::set_integer(Key::WindowHeight, height);
            Inhibit(false)
        });
    }

    fn setup_gactions(&self, sender: Sender<Action>) {
        // We need to upcast from ShApplicationWindow to gtk::ApplicationWindow, because ShApplicationWindow
        // currently doesn't implement GLib.ActionMap, since it's not supported in gtk-rs for subclassing (13-01-2020)
        let window = self.clone().upcast::<gtk::ApplicationWindow>();
        let app = window.get_application().unwrap();

        // win.refresh-data
        action!(
            window,
            "refresh-data",
            clone!(@strong sender => move |_, _| {
                send!(sender, Action::RefreshData);
            })
        );
        app.set_accels_for_action("win.refresh-data", &["<primary>r"]);

        // win.disconnect
        action!(
            window,
            "disconnect",
            clone!(@strong sender => move |_, _| {
                send!(sender, Action::Logout);
            })
        );

        // win.quit
        action!(
            window,
            "quit",
            clone!(@weak app => move |_, _| {
                app.quit();
            })
        );
        app.set_accels_for_action("win.quit", &["<primary>q"]);

        // win.about
        action!(
            window,
            "about",
            clone!(@weak window => move |_, _| {
                about_dialog::show_about_dialog(window);
            })
        );

        // win.show-preferences
        action!(
            window,
            "show-preferences",
            clone!(@weak window => move |_, _| {
                let settings_window = SettingsWindow::new(&window);
                settings_window.show();
            })
        );
    }

    pub fn show_login_page(&self, show: bool) {
        let self_ = ShApplicationWindowPrivate::from_instance(self);
        get_widget!(self_.window_builder, libhandy::ViewSwitcher, title_wide_switcher);
        get_widget!(self_.window_builder, libhandy::ViewSwitcher, title_narrow_switcher);
        get_widget!(self_.window_builder, libhandy::ViewSwitcherBar, view_switcher_bar);
        get_widget!(self_.menu_builder, gtk::ModelButton, refresh_mbutton);

        if show {
            title_wide_switcher.set_sensitive(false);
            title_narrow_switcher.set_sensitive(false);
            view_switcher_bar.set_sensitive(false);
            refresh_mbutton.set_sensitive(false);

            self.set_page("login");
        } else {
            title_wide_switcher.set_sensitive(true);
            title_narrow_switcher.set_sensitive(true);
            view_switcher_bar.set_sensitive(true);
            refresh_mbutton.set_sensitive(true);

            self.set_page("dashboard");
        }
    }

    pub fn show_notification(&self, notification: Rc<Notification>) {
        let self_ = ShApplicationWindowPrivate::from_instance(self);
        get_widget!(self_.window_builder, gtk::Overlay, content);

        // Remove previous notification
        if let Some(notification) = self_.current_notification.borrow_mut().take() {
            notification.hide();
        }

        notification.show(&content);
        *self_.current_notification.borrow_mut() = Some(notification);
    }

    fn add_page(&self, page: Box<dyn Page>) {
        let self_ = ShApplicationWindowPrivate::from_instance(self);

        get_widget!(self_.window_builder, gtk::Stack, stack);
        if let Some(title) = page.get_title() {
            stack.add_titled(&page.get_widget(), &page.get_name(), &title);
            stack.set_child_icon_name(&page.get_widget(), Some(&page.get_icon_name().unwrap()));
        } else {
            stack.add_named(&page.get_widget(), &page.get_name());
        }

        let len = self_.pages.borrow().len();
        self_.pages.borrow_mut().insert(len, page);
    }

    fn set_page(&self, page: &str) {
        let self_ = ShApplicationWindowPrivate::from_instance(self);
        get_widget!(self_.window_builder, gtk::Stack, stack);
        stack.set_visible_child_name(page);
    }

    pub fn refresh_data(&self) {
        let self_ = ShApplicationWindowPrivate::from_instance(self);
        for page in &*self_.pages.borrow() {
            page.refresh_data();
        }
    }
}
