// SmartHome - entity_dialog.rs
// Copyright (C) 2020  Felix Häcker <haeckerfelix@gnome.org>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use glib::Sender;
use gtk::prelude::*;
use libhandy::Dialog;

use crate::api::Entity;
use crate::app::Action;
use crate::utils;

pub struct EntityDialog {
    pub widget: Dialog,
    entity: Entity,

    builder: gtk::Builder,
    sender: Sender<Action>,
}

impl EntityDialog {
    pub fn new(sender: Sender<Action>, entity: Entity) -> Self {
        let builder = gtk::Builder::new_from_resource("/de/haeckerfelix/SmartHome/gtk/entity_dialog.ui");
        get_widget!(builder, Dialog, entity_dialog);

        let dialog = Self {
            widget: entity_dialog,
            entity,
            builder,
            sender,
        };

        dialog.setup_widgets();
        dialog.setup_signals();
        dialog.setup_information();
        dialog
    }

    fn setup_widgets(&self) {
        get_widget!(self.builder, gtk::ListBox, general_listbox);
        utils::set_listbox_placeholder(&general_listbox, "No information available");

        get_widget!(self.builder, gtk::ListBox, attributes_listbox);
        utils::set_listbox_placeholder(&attributes_listbox, "No attributes available");

        get_widget!(self.builder, gtk::ListBox, services_listbox);
        utils::set_listbox_placeholder(&services_listbox, "No services available");
    }

    fn setup_signals(&self) {}

    fn setup_information(&self) {
        get_widget!(self.builder, gtk::Image, entity_image);
        let icon_name = utils::get_entity_icon_name(&self.entity.domain);
        entity_image.set_from_icon_name(Some(icon_name), gtk::IconSize::LargeToolbar);

        get_widget!(self.builder, gtk::Label, entity_label);
        entity_label.set_text(&self.entity.get_friendly_name());

        get_widget!(self.builder, gtk::ListBox, general_listbox);
        utils::add_listbox_separators(&general_listbox);
        utils::add_details_row(&general_listbox, "State", &self.entity.state);
        utils::add_details_row(&general_listbox, "Entity ID", &self.entity.id);
        utils::add_details_row(&general_listbox, "Last changed", &self.entity.last_changed);
        utils::add_details_row(&general_listbox, "Last updated", &self.entity.last_updated);

        get_widget!(self.builder, gtk::ListBox, attributes_listbox);
        utils::add_listbox_separators(&attributes_listbox);
        for attribute in &self.entity.attributes {
            utils::add_details_row(&attributes_listbox, attribute.0, &attribute.1.to_string());
        }

        if let Some(services) = &self.entity.services {
            get_widget!(self.builder, gtk::ListBox, services_listbox);
            utils::add_listbox_separators(&services_listbox);
            for service in services {
                utils::add_details_row(&services_listbox, service.0, "");
            }
        }
    }

    pub fn show(&self) {
        let application = self.builder.get_application().unwrap();
        let window = application.get_active_window().unwrap();
        self.widget.set_transient_for(Some(&window));
        self.widget.set_visible(true);
    }
}
