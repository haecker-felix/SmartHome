// SmartHome - entity_tile.rs
// Copyright (C) 2020  Felix Häcker <haeckerfelix@gnome.org>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use glib::Sender;
use gtk::prelude::*;

use crate::api::Entity;
use crate::app::Action;
use crate::ui::EntityDialog;
use crate::utils;

pub struct EntityTile {
    pub widget: gtk::FlowBoxChild,
    entity: Entity,

    builder: gtk::Builder,
    sender: Sender<Action>,
}

impl EntityTile {
    pub fn new(sender: Sender<Action>, entity: Entity) -> Self {
        let builder = gtk::Builder::new_from_resource("/de/haeckerfelix/SmartHome/gtk/entity_tile.ui");
        get_widget!(builder, gtk::FlowBoxChild, entity_tile);

        get_widget!(builder, gtk::Label, title_label);
        get_widget!(builder, gtk::Label, state_label);
        title_label.set_text(&entity.get_friendly_name());
        state_label.set_text(&entity.state);

        get_widget!(builder, gtk::Image, domain_icon);
        let icon_name = utils::get_entity_icon_name(&entity.domain);
        domain_icon.set_from_icon_name(Some(icon_name), gtk::IconSize::LargeToolbar);

        // CSS styling
        let mut css = "";
        match entity.state.as_ref() {
            "on" => css = "turned-on",
            "off" => css = "turned-off",
            "home" => css = "person-home",
            "not_home" => css = "person-not-home",
            "open" => css = "cover-open",
            "closed" => css = "cover-closed",
            "unknown" => css = "unknown",
            _ => css = "emblem-system-symbolic",
        }
        let style_ctx = entity_tile.get_style_context();
        style_ctx.add_class(css);

        let tile = Self {
            widget: entity_tile,
            entity,
            builder,
            sender,
        };

        tile.setup_signals();
        tile
    }

    fn setup_signals(&self) {
        get_widget!(self.builder, gtk::Button, menu_button);
        menu_button.connect_clicked(clone!(@strong self.sender as sender, @strong self.entity as entity => move |_| {
            let entity_dialog = EntityDialog::new(sender.clone(), entity.clone());
            entity_dialog.show();
        }));
    }
}
