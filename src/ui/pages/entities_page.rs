// SmartHome - states_page.rs
// Copyright (C) 2020  Felix Häcker <haeckerfelix@gnome.org>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use glib::Sender;
use gtk::prelude::*;

use std::rc::Rc;

use crate::api::Client;
use crate::api::Entity;
use crate::app::Action;
use crate::i18n::*;
use crate::ui::pages::Page;
use crate::ui::EntityTile;
use crate::utils;

pub struct EntitiesPage {
    pub widget: gtk::Box,
    client: Rc<Client>,

    builder: gtk::Builder,
    sender: Sender<Action>,
}

impl EntitiesPage {
    pub fn new(sender: Sender<Action>, client: Rc<Client>) -> Self {
        let builder = gtk::Builder::new_from_resource("/de/haeckerfelix/SmartHome/gtk/pages/entities_page.ui");
        get_widget!(builder, gtk::Box, entities_page);

        let page = Self {
            widget: entities_page,
            client,
            builder,
            sender,
        };

        page.setup_widgets();
        page.setup_signals();
        page
    }

    fn setup_widgets(&self) {}

    fn setup_signals(&self) {}
}

impl Page for EntitiesPage {
    fn get_widget(&self) -> gtk::Box {
        self.widget.clone()
    }

    fn get_name(&self) -> String {
        "entities".to_string()
    }

    fn get_title(&self) -> Option<String> {
        Some(i18n("Entities"))
    }

    fn get_icon_name(&self) -> Option<String> {
        Some("view-grid".to_string())
    }

    fn clear_data(&self) {
        get_widget!(self.builder, gtk::FlowBox, entities_flowbox);
        utils::remove_all_items(&entities_flowbox);
    }

    fn refresh_data(&self) {
        self.clear_data();

        get_widget!(self.builder, gtk::FlowBox, entities_flowbox);
        let client = (*self.client).clone();
        let sender = self.sender.clone();
        let fut = async move {
            let services = client.clone().get_services().await.unwrap();
            let states = client.clone().get_states().await.unwrap();

            let mut entities = Vec::new();

            for state in states {
                let domain = state.get_domain();
                let services = if let Some(services) = services.get(&domain) { Some(services.clone()) } else { None };

                let entity = Entity::new(state, services);
                entities.insert(0, entity);
            }

            for entitiy in &entities {
                let tile = EntityTile::new(sender.clone(), entitiy.clone());
                entities_flowbox.add(&tile.widget);
            }
        };

        spawn!(fut);
    }
}
