mod dashboard_page;
pub use dashboard_page::DashboardPage;

mod entities_page;
pub use entities_page::EntitiesPage;

mod home_info_page;
pub use home_info_page::HomeInfoPage;

mod login_page;
pub use login_page::LoginPage;

pub trait Page {
    fn get_widget(&self) -> gtk::Box;
    fn get_name(&self) -> String;
    fn get_title(&self) -> Option<String>;
    fn get_icon_name(&self) -> Option<String>;

    fn refresh_data(&self);
    fn clear_data(&self);
}
