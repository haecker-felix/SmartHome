// SmartHome - states_page.rs
// Copyright (C) 2020  Felix Häcker <haeckerfelix@gnome.org>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use glib::Sender;
use gtk::prelude::*;
use url::Url;

use crate::api::ClientCredentials;
use crate::app::Action;
use crate::ui::pages::Page;

pub struct LoginPage {
    pub widget: gtk::Box,

    builder: gtk::Builder,
    sender: Sender<Action>,
}

impl LoginPage {
    pub fn new(sender: Sender<Action>) -> Self {
        let builder = gtk::Builder::new_from_resource("/de/haeckerfelix/SmartHome/gtk/pages/login_page.ui");
        get_widget!(builder, gtk::Box, login_page);

        let page = Self { widget: login_page, builder, sender };

        page.setup_widgets();
        page.setup_signals();
        page
    }

    fn setup_widgets(&self) {}

    fn setup_signals(&self) {
        get_widget!(self.builder, gtk::Button, connect_button);
        connect_button.connect_clicked(clone!(@weak self.builder as builder, @strong self.sender as sender => move |_|{
            get_widget!(builder, gtk::Entry, server_entry);
            get_widget!(builder, gtk::Entry, token_entry);

            if server_entry.get_text().is_some() && token_entry.get_text().is_some() {
                let server = Url::parse(server_entry.get_text().unwrap().as_str()).unwrap();
                let token = token_entry.get_text().unwrap().as_str().to_string();
                let credentials = ClientCredentials { server, token };
                send!(sender, Action::Login(credentials));
            }
        }));
    }
}

impl Page for LoginPage {
    fn get_widget(&self) -> gtk::Box {
        self.widget.clone()
    }

    fn get_name(&self) -> String {
        "login".to_string()
    }

    fn get_title(&self) -> Option<String> {
        None
    }

    fn get_icon_name(&self) -> Option<String> {
        None
    }

    fn clear_data(&self) {}

    fn refresh_data(&self) {}
}
