// SmartHome - states_page.rs
// Copyright (C) 2020  Felix Häcker <haeckerfelix@gnome.org>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use futures_util::future::FutureExt;
use glib::Sender;
use gtk::prelude::*;

use std::rc::Rc;

use crate::api::Client;
use crate::app::Action;
use crate::i18n::*;
use crate::ui::pages::Page;
use crate::utils;

pub struct HomeInfoPage {
    pub widget: gtk::Box,
    client: Rc<Client>,

    builder: gtk::Builder,
    sender: Sender<Action>,
}

impl HomeInfoPage {
    pub fn new(sender: Sender<Action>, client: Rc<Client>) -> Self {
        let builder = gtk::Builder::new_from_resource("/de/haeckerfelix/SmartHome/gtk/pages/home_info_page.ui");
        get_widget!(builder, gtk::Box, home_info_page);

        let page = Self {
            widget: home_info_page,
            client,
            builder,
            sender,
        };

        page.setup_widgets();
        page.setup_signals();
        page
    }

    fn setup_widgets(&self) {
        get_widget!(self.builder, gtk::ListBox, details_listbox);
        utils::set_listbox_placeholder(&details_listbox, "Loading");

        get_widget!(self.builder, gtk::ListBox, components_listbox);
        utils::set_listbox_placeholder(&components_listbox, "Loading");
    }

    fn setup_signals(&self) {
        // listbox separators
        get_widget!(self.builder, gtk::ListBox, details_listbox);
        utils::add_listbox_separators(&details_listbox);

        // listbox separators
        get_widget!(self.builder, gtk::ListBox, components_listbox);
        utils::add_listbox_separators(&components_listbox);
    }
}

impl Page for HomeInfoPage {
    fn get_widget(&self) -> gtk::Box {
        self.widget.clone()
    }

    fn get_name(&self) -> String {
        "home_info".to_string()
    }

    fn get_title(&self) -> Option<String> {
        Some(i18n("Home"))
    }

    fn get_icon_name(&self) -> Option<String> {
        Some("user-home-symbolic".to_string())
    }

    fn clear_data(&self) {
        get_widget!(self.builder, gtk::ListBox, details_listbox);
        utils::remove_all_items(&details_listbox);

        get_widget!(self.builder, gtk::ListBox, components_listbox);
        utils::remove_all_items(&components_listbox);
    }

    fn refresh_data(&self) {
        self.clear_data();

        get_widget!(self.builder, gtk::ListBox, details_listbox);
        get_widget!(self.builder, gtk::ListBox, components_listbox);

        let fut = (*self.client).clone().get_config().map(clone!(@weak details_listbox, @weak components_listbox => move |c|{
            let config = c.unwrap();
            utils::add_details_row(&details_listbox, "Location Name", &config.location_name);
            utils::add_details_row(&details_listbox, "Time Zone", &config.time_zone);
            utils::add_details_row(&details_listbox, "Version", &config.version);

            for name in &config.components {
                utils::add_details_row(&components_listbox, &name, "");
            }
        }));
        spawn!(fut);
    }
}
