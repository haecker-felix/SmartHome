// SmartHome - states_page.rs
// Copyright (C) 2020  Felix Häcker <haeckerfelix@gnome.org>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use glib::Sender;
use gtk::prelude::*;

use std::rc::Rc;

use crate::api::Client;
use crate::app::Action;
use crate::i18n::*;
use crate::ui::pages::Page;

pub struct DashboardPage {
    pub widget: gtk::Box,
    client: Rc<Client>,

    builder: gtk::Builder,
    sender: Sender<Action>,
}

impl DashboardPage {
    pub fn new(sender: Sender<Action>, client: Rc<Client>) -> Self {
        let builder = gtk::Builder::new_from_resource("/de/haeckerfelix/SmartHome/gtk/pages/dashboard_page.ui");
        get_widget!(builder, gtk::Box, dashboard_page);

        let page = Self {
            widget: dashboard_page,
            client,
            builder,
            sender,
        };

        page.setup_widgets();
        page.setup_signals();
        page
    }

    fn setup_widgets(&self) {}

    fn setup_signals(&self) {}
}

impl Page for DashboardPage {
    fn get_widget(&self) -> gtk::Box {
        self.widget.clone()
    }

    fn get_name(&self) -> String {
        "dashboard".to_string()
    }

    fn get_title(&self) -> Option<String> {
        Some(i18n("Dashboard"))
    }

    fn get_icon_name(&self) -> Option<String> {
        Some("applications-system-symbolic".to_string())
    }

    fn clear_data(&self) {}

    fn refresh_data(&self) {}
}
