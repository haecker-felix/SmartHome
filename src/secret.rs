// SmartHome - secret.rs
// Copyright (C) 2020  Felix Häcker <haeckerfelix@gnome.org>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

// Original source: Read-It-Later
// https://gitlab.gnome.org/World/read-it-later/-/raw/master/src/models/secret.rs

use secret_service::EncryptionType;
use secret_service::{SecretService, SsError};

use std::rc::Rc;

use crate::api::ClientCredentials;

pub struct SecretManager {
    service: Rc<SecretService>,
}

impl SecretManager {
    fn new() -> Result<Self, SsError> {
        let service = Rc::new(SecretService::new(EncryptionType::Dh)?);
        let collection = service.get_default_collection()?;
        if collection.is_locked()? {
            collection.unlock()?;
        }
        Ok(Self { service })
    }

    pub fn reset_credentials(server: url::Url) -> Result<(), SsError> {
        let service = Self::new()?;
        let items = service.service.search_items(vec![("home-assistant-server", server.to_string().as_str())])?;

        for item in items.iter() {
            item.delete()?;
        }
        Ok(())
    }

    pub fn store_credentials(credentials: ClientCredentials) -> Result<(), SsError> {
        let service = Self::new()?;
        let collection = service.service.get_default_collection()?;

        if Self::get_credentials(credentials.clone().server).is_none() {
            let server = credentials.server.clone().to_string();
            let attributes = vec![("home-assistant-server", server.as_str())];
            let description = format!("Smart Home API Token for {}", credentials.server.to_string());
            collection.create_item(&description, attributes, &credentials.token.into_bytes(), false, "text/plain").unwrap();
        }

        Ok(())
    }

    pub fn get_credentials(server: url::Url) -> Option<ClientCredentials> {
        let service = Self::new().ok()?;
        let items = service.service.search_items(vec![("home-assistant-server", server.to_string().as_str())]).ok()?;

        if let Some(item) = items.get(0) {
            let value = item.get_secret().ok()?;
            let credentials = ClientCredentials {
                server: server,
                token: String::from_utf8(value).unwrap(),
            };
            return Some(credentials);
        }

        None
    }
}
