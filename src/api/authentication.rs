// SmartHome - authentication.rs
// Copyright (C) 2020  Felix Häcker <haeckerfelix@gnome.org>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use url::Url;

#[derive(Debug, Clone, serde_derive::Serialize)]
pub struct HaAuthMessage {
    #[serde(rename(serialize = "type"))]
    pub mtype: String,
    pub access_token: String,
}

#[derive(Clone, Debug)]
pub struct ClientCredentials {
    pub server: Url,
    pub token: String,
}

impl ClientCredentials {
    pub fn get_auth_message(&self) -> HaAuthMessage {
        HaAuthMessage {
            mtype: "auth".to_string(),
            access_token: self.token.clone(),
        }
    }

    pub fn get_websocket_url(&self) -> Url {
        let host = self.server.host_str().as_ref().unwrap().to_string();
        let mut port = self.server.port().as_ref().unwrap_or(&0).to_string();
        if port == "0" {
            port = "".to_string();
        } else {
            port = format!(":{}", &port);
        }

        if self.server.scheme() == "https" {
            let url = format!("wss://{}{}/api/websocket", host, port).to_string();
            Url::parse(&url).unwrap()
        } else {
            let url = format!("ws://{}{}/api/websocket", host, port).to_string();
            Url::parse(&url).unwrap()
        }
    }
}
