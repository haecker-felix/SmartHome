// SmartHome - event.rs
// Copyright (C) 2020  Felix Häcker <haeckerfelix@gnome.org>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use crate::api::State;

#[derive(Default, Debug, Clone, PartialEq, serde_derive::Deserialize)]
pub struct Event {
    pub event_type: String,
    pub origin: String,
    pub time_fired: String,
    pub data: EventData,
}

#[derive(Default, Debug, Clone, PartialEq, serde_derive::Deserialize)]
pub struct EventData {
    pub entity_id: String,
    pub old_state: State,
    pub new_state: State,
}
