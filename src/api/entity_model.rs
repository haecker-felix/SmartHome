// SmartHome - entity_model.rs
// Copyright (C) 2020  Felix Häcker <haeckerfelix@gnome.org>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::collections::HashMap;

use crate::api::*;

pub struct EntityModel {}

impl EntityModel {
    pub fn new() -> Self {
        EntityModel {}
    }

    pub fn set_states(&mut self, states: Vec<State>) {}

    pub fn set_services(&mut self, services: HashMap<String, HashMap<String, Service>>) {}
}
