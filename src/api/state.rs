// SmartHome - state.rs
// Copyright (C) 2020  Felix Häcker <haeckerfelix@gnome.org>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::collections::HashMap;

#[derive(Default, Debug, Clone, PartialEq, serde_derive::Deserialize)]
pub struct State {
    pub attributes: HashMap<String, serde_json::Value>,
    pub context: HashMap<String, serde_json::Value>,
    pub entity_id: String,
    pub last_changed: String,
    pub last_updated: String,
    pub state: String,
}

impl State {
    pub fn get_domain(&self) -> String {
        let mut split = self.entity_id.split(".");
        let vec: Vec<&str> = split.collect();
        vec[0].to_string()
    }
}
