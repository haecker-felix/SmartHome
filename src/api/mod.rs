// SmartHome - mod.rs
// Copyright (C) 2020  Felix Häcker <haeckerfelix@gnome.org>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

static STATES: &str = "api/states";
static CONFIG: &str = "api/config";
static SERVICES: &str = "api/services";

mod authentication;
mod client;
mod config;
mod entity;
mod entity_model;
mod error;
mod event;
mod service;
mod state;
pub mod websocket_client;

pub use authentication::ClientCredentials;
pub use client::Client;
pub use config::Config;
pub use entity::Entity;
pub use entity_model::EntityModel;
pub use error::Error;
pub use event::{Event, EventData};
pub use service::{Service, ServiceField};
pub use state::State;
pub use websocket_client::WebSocketClient;
pub use websocket_client::WebSocketMessage;
