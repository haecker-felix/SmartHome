// SmartHome - websocket_client.rs
// Copyright (C) 2020  Felix Häcker <haeckerfelix@gnome.org>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use async_tungstenite::gio::IOStreamAsyncReadWrite;
use async_tungstenite::WebSocketStream;
use async_tungstenite::{gio::connect_async, tungstenite::Message};
use futures::prelude::*;
use gio::SocketConnection;
use glib::Sender;
use tungstenite::error::Error;

use std::cell::RefCell;
use std::collections::HashMap;
use std::rc::Rc;

use crate::api::*;
use crate::app::Action;

#[derive(Debug, Clone, serde_derive::Deserialize)]
#[serde(tag = "type")]
enum HaMessage {
    #[serde(rename(deserialize = "auth_required"))]
    AuthRequiredMessage,
    #[serde(rename(deserialize = "auth_ok"))]
    AuthOkMessage,
    #[serde(rename(deserialize = "auth_invalid"))]
    AuthFailureMessage,

    #[serde(rename(deserialize = "event"))]
    EventMessage { id: i64, event: Event },
    #[serde(rename(deserialize = "result"))]
    ResultMessage { id: i64, success: bool, result: serde_json::Value },

    #[serde(rename(deserialize = "pong"))]
    PingPongMessage,
}

#[derive(Debug, Clone)]
pub enum WebSocketMessage {
    States(Vec<State>),
    Services(HashMap<String, HashMap<String, Service>>),
}

pub struct WebSocketClient {
    credentials: RefCell<Option<ClientCredentials>>,
    ws_stream: RefCell<Option<WebSocketStream<IOStreamAsyncReadWrite<SocketConnection>>>>,

    disconnect: RefCell<bool>,

    sender: Sender<Action>,
}

impl WebSocketClient {
    pub fn new(sender: Sender<Action>) -> Rc<Self> {
        let credentials = RefCell::new(None);
        let ws_stream = RefCell::new(None);
        let disconnect = RefCell::new(false);

        let client = Rc::new(Self {
            credentials,
            ws_stream,
            disconnect,
            sender,
        });

        client
    }

    pub fn set_credentials(&self, credentials: Option<ClientCredentials>) {
        *self.credentials.borrow_mut() = credentials;
    }

    pub async fn connect(self: Rc<Self>) -> Result<(), Error> {
        let ws_stream = connect_async(self.credentials.borrow().as_ref().unwrap().get_websocket_url()).await?.0;

        *self.ws_stream.borrow_mut() = Some(ws_stream);
        *self.disconnect.borrow_mut() = false;

        self.clone().do_work().await.unwrap();

        Ok(())
    }

    pub async fn disconnect(self: Rc<Self>) -> Result<(), Error> {
        *self.disconnect.borrow_mut() = true;
        Ok(())
    }

    async fn do_work(self: Rc<Self>) -> Result<(), Error> {
        loop {
            if *self.disconnect.borrow() == true {
                *self.ws_stream.borrow_mut() = None;
                debug!("Disconnected.");
                break;
            }

            let mut message = None;
            match self.ws_stream.borrow_mut().as_mut().unwrap().next().await {
                Some(m) => match m {
                    Ok(m) => message = Some(m),
                    Err(error) => warn!("Message error: {}", error.to_string()),
                },
                None => {
                    debug!("No further messages to receive.");
                    break;
                }
            }

            if let Some(message) = message {
                self.clone().process_websocket_message(message).await;
            }
        }

        debug!("WebSocket loop stopped");
        Ok(())
    }

    async fn process_websocket_message(self: Rc<Self>, m: Message) {
        match m {
            Message::Text(text) => {
                //dbg!(&text);
                let message: HaMessage = serde_json::from_str(&text).unwrap();
                self.clone().process_homeassistant_message(message).await;
            }
            Message::Close(_) => {
                debug!("WebSocket connection closed.");
            }
            _ => warn!("Unknown message type/data"),
        }
    }

    async fn process_homeassistant_message(self: Rc<Self>, m: HaMessage) {
        match m {
            HaMessage::AuthRequiredMessage => {
                let auth_message = self.credentials.borrow().as_ref().unwrap().get_auth_message();
                self.clone().send_message(&serde_json::to_string(&auth_message).unwrap()).await.unwrap();
            }
            HaMessage::AuthOkMessage => {
                // TODO: Don't hardcode message id

                // subscribe to events
                self.clone().send_message(r#"{"id": 10,"type": "subscribe_events"}"#).await.unwrap();

                // fetch all states
                self.clone().send_message(r#"{"id": 20,"type": "get_states"}"#).await.unwrap();

                // fetch all services
                self.clone().send_message(r#"{"id": 30,"type": "get_services"}"#).await.unwrap();
            }
            HaMessage::EventMessage { id, event } => debug!("Received event"),
            HaMessage::ResultMessage { id, success, result } => self.parse_result_message(result),
            HaMessage::PingPongMessage => debug!("Received pong"),
            _ => (),
        }
    }

    fn parse_result_message(self: Rc<Self>, value: serde_json::Value) {
        let states: Vec<State> = serde_json::from_str(&value.to_string()).unwrap_or_default();
        if !states.is_empty() {
            let message = WebSocketMessage::States(states);
            send!(self.sender, Action::ProcessWebSocketMessage(message));
            return;
        }

        let services: HashMap<String, HashMap<String, Service>> = serde_json::from_str(&value.to_string()).unwrap_or_default();
        if !services.is_empty() {
            let message = WebSocketMessage::Services(services);
            send!(self.sender, Action::ProcessWebSocketMessage(message));
            return;
        }

        warn!("Unknown result type: {:?}", value.to_string());
    }

    async fn send_message(self: Rc<Self>, data: &str) -> Result<(), Error> {
        if let Some(ws_stream) = &mut *self.ws_stream.borrow_mut() {
            let message = Message::text(data);

            debug!("Send message: {:?}", message);
            ws_stream.send(message).await?;
        }
        Ok(())
    }
}
