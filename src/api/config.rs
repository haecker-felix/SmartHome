// SmartHome - state.rs
// Copyright (C) 2020  Felix Häcker <haeckerfelix@gnome.org>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#[derive(Default, Debug, Clone, PartialEq, serde_derive::Deserialize)]
pub struct Config {
    pub components: Vec<String>,
    pub config_dir: String,
    pub config_source: String,
    pub elevation: i64,
    pub latitude: f64,
    pub location_name: String,
    pub longitude: f64,
    pub safe_mode: bool,
    pub time_zone: String,
    pub unit_system: UnitSystem,
    pub version: String,
    pub whitelist_external_dirs: Vec<String>,
}

#[derive(Default, Debug, Clone, PartialEq, serde_derive::Deserialize)]
pub struct UnitSystem {
    pub length: String,
    pub mass: String,
    pub pressure: String,
    pub temperature: String,
    pub volume: String,
}
