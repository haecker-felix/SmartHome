// SmartHome - entitiy.rs
// Copyright (C) 2020  Felix Häcker <haeckerfelix@gnome.org>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::collections::HashMap;

use crate::api::*;

#[derive(Debug, Clone, PartialEq)]
pub struct Entity {
    pub id: String,
    pub domain: String,
    pub name: String,

    pub last_updated: String,
    pub last_changed: String,

    pub attributes: HashMap<String, serde_json::Value>,
    pub services: Option<HashMap<String, Service>>,

    pub state: String,
}

impl Entity {
    pub fn new(state: State, services: Option<HashMap<String, Service>>) -> Self {
        let id = state.entity_id.clone();
        let mut tmp = state.entity_id.split(".");
        let domain = tmp.next().unwrap().to_string();
        let name = tmp.next().unwrap().to_string();

        let last_changed = state.last_changed;
        let last_updated = state.last_updated;

        let attributes = state.attributes;
        let services = services;

        let state = state.state;

        Self {
            id,
            domain,
            name,
            last_updated,
            last_changed,
            attributes,
            services,
            state,
        }
    }

    pub fn get_friendly_name(&self) -> String {
        if let Some(name) = self.attributes.get("friendly_name") {
            return name.as_str().unwrap().to_string();
        } else {
            return self.name.clone();
        }
    }
}
