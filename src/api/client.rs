// SmartHome - client.rs
// Copyright (C) 2020  Felix Häcker <haeckerfelix@gnome.org>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use isahc::prelude::*;
use url::Url;

use std::cell::RefCell;
use std::collections::HashMap;

use crate::api::*;
use crate::config;

#[derive(Clone)]
pub struct Client {
    credentials: RefCell<Option<ClientCredentials>>,
}

impl Client {
    pub fn new() -> Self {
        let credentials = RefCell::new(None);
        Self { credentials }
    }

    pub fn set_credentials(&self, credentials: Option<ClientCredentials>) {
        *self.credentials.borrow_mut() = credentials;
    }

    pub async fn get_states(self) -> Result<Vec<State>, Error> {
        let url = self.build_url(STATES, None)?;
        let data = self.send_message(url).await?;
        let states: Vec<State> = serde_json::from_str(data.as_str())?;

        Ok(states)
    }

    pub async fn get_services(self) -> Result<HashMap<String, HashMap<String, Service>>, Error> {
        let url = self.build_url(SERVICES, None)?;
        let data = self.send_message(url).await?;

        let mut result: HashMap<String, HashMap<String, Service>> = HashMap::new();
        //let domain_service_list: Vec<DomainServiceList> = serde_json::from_str(data.as_str())?;

        //for dsl in domain_service_list {
        //    result.insert(dsl.domain, dsl.services);
        //}

        Ok(result)
    }

    pub async fn get_config(self) -> Result<Config, Error> {
        let url = self.build_url(CONFIG, None)?;
        let data = self.send_message(url).await?;
        let conf: Config = serde_json::from_str(data.as_str())?;

        Ok(conf)
    }

    // Create and send message, return the received data.
    async fn send_message(self, url: Url) -> Result<String, Error> {
        if let Some(credentials) = self.credentials.borrow().clone() {
            let useragent = format!("{}/{}-{}", config::PKGNAME, config::VERSION, config::PROFILE);

            let mut request = Request::builder();
            let token = format!("Bearer {}", credentials.token);
            request
                .uri(url.to_string())
                .header("User-Agent", useragent)
                .header("Authorization", token)
                .header("Content-Type", "application/json");

            let response = isahc::send_async(request.body(()).unwrap()).await?.text_async().await?;
            return Ok(response);
        }
        Err(Error::MissingCredentials)
    }

    fn build_url(&self, param: &str, options: Option<&str>) -> Result<Url, Error> {
        if let Some(credentials) = self.credentials.borrow().clone() {
            let mut url = credentials.server.join(param)?;
            if let Some(options) = options {
                url.set_query(Some(options))
            }
            return Ok(url);
        }

        Err(Error::MissingCredentials)
    }
}
