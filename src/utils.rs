// SmartHome - utils.rs
// Copyright (C) 2020  Felix Häcker <haeckerfelix@gnome.org>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use glib::{self, object::WeakRef};
use gtk::prelude::*;

pub fn get_entity_icon_name(domain: &str) -> &str {
    match domain {
        "sensor" => "dialog-information-symbolic",
        "media_player" => "emblem-music-symbolic",
        "cover" => "image-x-generic-symbolic",
        "sun" => "weather-few-clouds-symbolic",
        "light" => "weather-clear-symbolic",
        _ => "emblem-system-symbolic",
    }
}

// If you want to know more about lazy loading, you should read these:
// - https://en.wikipedia.org/wiki/Lazy_loading
// - https://blogs.gnome.org/ebassi/documentation/lazy-loading/comment-page-1/
//
// Source: gnome-podcasts (GPLv3)
// https://gitlab.gnome.org/World/podcasts/blob/7856b6fd27cb071583b87f55f3e47d9d8af9acb6/podcasts-gtk/src/utils.rs
pub(crate) fn lazy_load<T, C, F, W>(data: T, container: WeakRef<C>, mut contructor: F)
where
    T: IntoIterator + 'static,
    T::Item: 'static,
    C: IsA<glib::Object> + ContainerExt + 'static,
    F: FnMut(T::Item) -> W + 'static,
    W: IsA<gtk::Widget> + WidgetExt,
{
    let func = move |x| {
        let container = match container.upgrade() {
            Some(c) => c,
            None => return,
        };

        let widget = contructor(x);
        container.add(&widget);
        widget.show();
    };
    lazy_load_full(data, func);
}

pub(crate) fn lazy_load_full<T, F>(data: T, mut func: F)
where
    T: IntoIterator + 'static,
    T::Item: 'static,
    F: FnMut(T::Item) + 'static,
{
    let mut data = data.into_iter();
    gtk::idle_add(move || data.next().map(|x| func(x)).map(|_| glib::Continue(true)).unwrap_or_else(|| glib::Continue(false)));
}

// Removes all child items
pub fn remove_all_items<T>(container: &T)
where
    T: IsA<gtk::Container> + gtk::ContainerExt,
{
    let children = container.get_children();
    for widget in children {
        container.remove(&widget);
        widget.destroy();
    }
}

pub fn add_details_row(listbox: &gtk::ListBox, title: &str, text: &str) {
    let builder = gtk::Builder::new_from_resource("/de/haeckerfelix/SmartHome/gtk/pages/home_info_page.ui");
    get_widget!(builder, gtk::ListBoxRow, row);
    get_widget!(builder, gtk::Label, title_label);
    get_widget!(builder, gtk::Label, text_label);
    title_label.set_text(title);
    text_label.set_text(text);

    listbox.add(&row);
}

pub fn add_listbox_separators(listbox: &gtk::ListBox) {
    listbox.set_header_func(Some(Box::new(|row, before| {
        if before.is_some() {
            let separator = gtk::Separator::new(gtk::Orientation::Horizontal);
            row.set_header(Some(&separator));
        }
    })));
}

pub fn set_listbox_placeholder(listbox: &gtk::ListBox, placeholder: &str) {
    let placeholder = gtk::Label::new(Some(placeholder));
    placeholder.set_visible(true);
    placeholder.set_property_margin(12);

    listbox.set_placeholder(Some(&placeholder));
}
