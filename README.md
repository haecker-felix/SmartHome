# Smart Home
Control your smart home

![Logo](https://gitlab.gnome.org/World/SmartHome/raw/master/data/icons/hicolor/scalable/apps/de.haeckerfelix.SmartHome.svg)

## Getting in Touch
If you have any questions regarding the use or development of Smart Home,
want to discuss design or simply hang out, please join us on our [#smarthome:matrix.org](https://matrix.to/#/#smarthome:matrix.org) channel.

## FAQ

- **How I can get debug information?**
    Run Smart Home using `RUST_BACKTRACE=1 RUST_LOG=smarthome=debug flatpak run de.haeckerfelix.SmartHome` (`.Devel`).

## Building
### Building with Flatpak + GNOME Builder
Smart Home can be built and run with [GNOME Builder](https://wiki.gnome.org/Apps/Builder) >= 3.28.
Just clone the repo and hit the run button!

You can get Builder from [here](https://wiki.gnome.org/Apps/Builder/Downloads), and the Rust Nightly Flatpak SDK (if necessary) from [here](https://haeckerfelix.de/~repo/)

### Building it manually
1. `git clone https://gitlab.gnome.org/World/SmartHome.git`
2. `cd SmartHome`
3. `meson --prefix=/usr build`
4. `ninja -C build`
5. `sudo ninja -C build install`

You need following dependencies to build Smart Home:
- Rust 1.39 or later
- GTK 3.24 or later
- OpenSSL
- [libhandy](https://source.puri.sm/Librem5/libhandy)
- [Meson Buildsystem](https://mesonbuild.com/)

If you need help to build Smart Home, please don't hesitate to ask [here](https://matrix.to/#/#smarthome:matrix.org)!

## Code Of Conduct
We follow the [GNOME Code of Conduct](/CODE_OF_CONDUCT.mdmd).
All communications in project spaces are expected to follow it.
